package result

import (
	"errors"
)

const (
	HttpStatusInternalServerError = 500
	HttpStatusOk                  = 200
)

var (
	ErrorSuccessfulResultWithError        = errors.New("invalidOperation: A result cannot be successful and contain an error")
	ErrorFailingResultWithoutErrorMessage = errors.New("invalidOperation: A failing result needs to contain an error message")
	ErrorNoValueOnFailedResult            = errors.New("can't retrieve the value from a failed result")
)

func NewResult(isSuccess bool, err string, value interface{}) (*Result, error) {
	if isSuccess == true && len(err) > 0 {
		return nil, ErrorSuccessfulResultWithError
	}

	if isSuccess == false && len(err) == 0 {
		return nil, ErrorFailingResultWithoutErrorMessage
	}

	result := &Result{
		IsSuccess: isSuccess,
		Error:     nil,
		Data:      value,
	}

	if len(err) == 0 {
		result.SetStatus(HttpStatusOk)

		return result, nil
	}

	result.Error = &Error{Message: err}
	result.SetStatus(HttpStatusInternalServerError)

	return result, nil
}

func NewResultWithKey(isSuccess bool, err string, errKey string, value interface{}) (*Result, error) {
	if isSuccess == true && len(err) > 0 {
		return nil, ErrorSuccessfulResultWithError
	}

	if isSuccess == false && len(err) == 0 {
		return nil, ErrorFailingResultWithoutErrorMessage
	}

	result := &Result{
		IsSuccess: isSuccess,
		Error:     nil,
		Data:      value,
	}

	if len(err) == 0 && len(errKey) == 0 {
		result.SetStatus(HttpStatusOk)

		return result, nil
	}

	result.Error = &Error{Message: err, Key: errKey}
	result.SetStatus(HttpStatusInternalServerError)

	return result, nil
}

func NewErrorResult(err *Error) (*Result, error) {
	result := &Result{
		IsSuccess: false,
		Error:     err,
		Data:      nil,
	}

	result.SetStatus(err.GetStatus())

	return result, nil
}

func Ok(value ...interface{}) *Result {
	var result *Result
	var err error

	if len(value) == 0 {
		result, err = NewResult(true, "", nil)
	} else {
		result, err = NewResult(true, "", value[0])
	}

	if err != nil {
		result = &Result{
			IsSuccess: false,
			Error:     &Error{Message: err.Error(), Key: "unable_to_create_result"},
			Data:      nil,
		}

		result.SetStatus(HttpStatusInternalServerError)

		return result
	}

	result.SetStatus(HttpStatusOk)

	return result
}

func Fail(errorMessage string) *Result {
	result, err := NewResult(false, errorMessage, nil)

	if err != nil {
		result = &Result{
			IsSuccess: false,
			Error:     &Error{Message: err.Error(), Key: "unable_to_create_result"},
			Data:      nil,
		}

		result.SetStatus(HttpStatusInternalServerError)

		return result
	}

	result.SetStatus(result.Error.GetStatus())

	return result
}

func FailWithKey(errorMessage string, errorKey string) *Result {
	result, err := NewResultWithKey(false, errorMessage, errorKey, nil)

	if err != nil {
		result = &Result{
			IsSuccess: false,
			Error:     &Error{Message: err.Error(), Key: "unable_to_create_result"},
			Data:      nil,
		}

		result.SetStatus(HttpStatusInternalServerError)

		return result
	}

	result.SetStatus(result.Error.GetStatus())

	return result
}

func FailError(errorWithKey Error) *Result {
	result, err := NewErrorResult(&errorWithKey)

	if err != nil {
		result = &Result{
			IsSuccess: false,
			Error:     &Error{Message: err.Error(), Key: "unable_to_create_result"},
			Data:      nil,
		}

		result.SetStatus(HttpStatusInternalServerError)

		return result
	}

	return result
}

func Combine(results []Result) *Result {
	for _, result := range results {
		if result.IsSuccess == false {
			return &result
		}
	}

	return Ok()
}
