package result

import (
	"testing"
)

func TestFailWithKey(t *testing.T) {
	t.Run("with non empty arguments", func(t *testing.T) {
		errMessage := "error message"
		errKey := "error_key"

		got := FailWithKey(errMessage, errKey)

		want := Result{
			IsSuccess: false,
			Error: &Error{
				Message: errMessage,
				Key:     errKey,
				Status:  500,
			},
			Data:      nil,
			status:    500,
			RequestId: "",
		}
		want.RequestId = got.RequestId

		if got.Error.Message != errMessage ||
			got.Error.Key != errKey ||
			got.IsSuccess != false ||
			got.Data != nil ||
			got.GetStatus() != 500 {
			t.Errorf("Not valid fail result with key")
		}
	})
}
