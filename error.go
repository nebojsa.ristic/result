package result

type Error struct {
	Message string `json:"message,omitempty" example:""`
	Key     string `json:"key,omitempty" example:""`
	Status  int    `json:"-" example:"500"`
}

func (err Error) Error() string {
	return err.Key + "-" + err.Message
}

func (err *Error) SetStatus(status int) Error {
	err.Status = status

	return *err
}

func (err Error) GetStatus() int {
	if err.Status == 0 {
		return HttpStatusInternalServerError
	}

	return err.Status
}
