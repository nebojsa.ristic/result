package result

import (
	"encoding/json"
)

type Result struct {
	IsSuccess bool   `json:"isSuccess" example:"true"`
	Error     *Error `json:"error,omitempty"`
	Data      interface{}
	status    int
	RequestId string
}

func (result Result) GetValue() (interface{}, error) {
	if result.IsSuccess == false {
		return nil, ErrorNoValueOnFailedResult
	}

	return result.Data, nil
}

func (result *Result) SetRequestId(requestId string) *Result {
	result.RequestId = requestId

	return result
}

func (result *Result) SetStatus(status int) *Result {
	result.status = status

	return result
}

func (result Result) GetStatus() int {
	return result.status
}

type MarshalResultFormat struct {
	IsSuccess bool        `json:"isSuccess"`
	Error     *Error      `json:"error,omitempty"`
	Data      interface{} `json:"data,omitempty"`
	RequestId string      `json:"requestId,omitempty"`
	Status    int         `json:"status"`
}

func (d Result) MarshalJSON() ([]byte, error) {
	var mrf MarshalResultFormat

	mrf.IsSuccess = d.IsSuccess

	if d.Error != nil {
		mrf.Error = &Error{
			Message: d.Error.Message,
			Key:     d.Error.Key,
			Status:  d.Error.GetStatus(),
		}
	}

	mrf.Data = d.Data
	mrf.RequestId = d.RequestId
	mrf.Status = d.GetStatus()

	return json.Marshal(mrf)
}
